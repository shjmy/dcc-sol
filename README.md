# DCC-Sol

## Manual Steps

The following section provides instructions on how to setup the server and deploy changes manually.

### Server Setup

This instruction was written with the expectation that the server will be an AWS EC2 Instance. These steps will be written with some assumption that you have either worked with AWS or know what it is.

Note: There are some UI Changes to the AWS Console so please use these steps more as guidelines.

1. Make sure you are in the **N. Virginia** Region
  * This dropdown menu should be located on the upper right of the page. Left of the Support dropdown.
2. Navigate to the **EC2** Service
3. Launch a new instance using **Launch instance**
  * If prompted with multiple choices then choose the basic Launch instance. Not the template one.
4. Select **Amazon Linux 2 AMI (HVM), SSD Volume Type** with the **64-bit (x86)** option.
  * The AMI ID should be: ami-00068cd7555f543d5 (64-bit x86)
5. Select **t2.micro** option and click **Next: Configure Instance Details**
  * Should say Free tier eligible
6. Scroll to the bottom of the Configure Instance Details page.
  * Enter the contents of step 6.2 in the **User data**
  * ```
     #!/bin/bash
     yum update -y
     amazon-linux-extras install -y nginx1.12
     service nginx start
     ```

  * Click **Next: Add Storage**
7. At the storage page click **Next: Add Tags** as there are no changes to be made here.
8. Click **Add Tag** and enter the following
  * For the Key Field
    * **Name**
  * For the Value Field
    * **dcc-sol**
  * **Click Next: Configure Security Group**
9. For the Security Group please make sure the group has the following.
  * Security Group Name is changed to **dcc-sol**
  * Change the description so launch-wizard-## is replaced with your full name.
  * The existing SSH source should be the site IP address.
    * Click the Dropdown menu in source and select **MyIP**
  * Add a HTTP rule
    * It's ok for this to accept all communications.
10. Click **Review and Launch**.
11. Click **Launch**
12. When prompted for a key pair select an existing key. Talk to your team lead for details.

### Deploy
If you are reading this then something is possibly very wrong and you need to deploy a change to the server manually.
1. Make sure you have the correct credentials. Talk to your team lead for details.
2. SSH into the host server using the proper ip address (Not listed since its subject to change)
  * since we are using amazon linux 2 please use ec2-user as the username
3. Stop the nginx instance using `sudo service nginx stop`
4. Using SCP or an equivalent transfer the new index.html to the server.
5. Replace the existing index.html at `/usr/share/nginx/html/`
6. Once replaced start the service again using `sudo service nginx start`

### Status Checker
There is a `python3` script named `checker.py` that can check to see if the site returns a status 200. Replace the sitename/ip address as needed.
* `python3 checker.py`

## Automated
### Create Infrastructure with Cloudformation + AWS CLI

`cloudformation.yml` along with the contents of the `./configs` directory should contain the necessary components to create a cloudformation stack. You can run the command below from the root of the project folder and it will create the following resources
* An EC2 instance of specified size with nginx installed and started
  * The instance size and keypair can be changed in `./configs/parameters.json`
  * The tags including instance namges can be changed in `./configs/tags.json`
* A new security group will be created and attached to the EC2 instance


```
aws cloudformation create-stack --stack-name dcc-sol --template-body file://cloudformation.yml --parameters file://configs/parameters.json --tags file://configs/tags.json
```

### Deploy 
The codebase for the actual website exists in the `DCC-Sol-Content` repository. In order to update the new page you need to simply push the new page to the master branch. At the moment, since this is a demo, only the index.html will be updated. Once the framework and scope of the site is planned the deployment script can be modified. Currently there is no testing in place nor a concept of a staging server but these things can be added later.
* Push new **index.html** file to the **master branch** of the **DCC-Sol-Content** repository

### Status Checker
An automated status checker exists through gitlab-cicd. Check `.gitlab-ci.yml` for details. On the respository page there is a schedule section that runs the pipeline script every hour. This can be configued to any time interval. Look at the CI/CD secion to see the log of statuses since the start of this automation. 
